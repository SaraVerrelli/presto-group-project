<x-layout>
  
  
  
  {{-- Header --}}
  <div class="contanier bgOrange shadow">
    <div class="row mb-4 mt-3 align-items-center">
      <div class="col-12 col-md-8 offset-md-2 p-4">
        @if (session('message')) 
        <div class="alert alertCustom Bradius"> 
          {{ session('message') }}
        </div>       
        @endif
      </div>
    </div>
    
    <div class="row mt-5 align-items-center">
      <div class="col-12 col-md-5 offset-1 p-4">

        {{-- form per la ricerca per testo --}}
        
      <h1 class="mt-3 mb-2 tWhite "><strong>{{ __('ui.homeH1')}}</strong></h1>
        <h3 class="mt-1 tWhite ">{{ __('ui.homeHeaderH3')}}<br>{{ __('ui.homeHeaderh32')}}</h3>
        <h6 class="tBlue"><strong>{{ __('ui.homeHeaderH6')}}</strong></h6>
        
        <div class="col-8 col-md-11 mt-5">
          <form class="d-flex" action="{{route('search')}}" method="GET">
            <input name="q" type="text" placeholder="{{ __('ui.homeBtnCerca')}}" class=" me-2 searchLabel">
            <button class="btn btnSearch" type="submit"><i class="fas fa-search"></i></button>
          </form>
          
        </div>
      </div>
      <div class="col-12 col-md-5   ">
        
        <img class="img-fluid" src="/home/imgHome.png
        " alt="">
        
      </div>
    </div>
  </div>  
      
   {{--comunicazione categorie  --}}
   <div class="container-fluid bgWhite shadow">
    <div class="row">
      <div class="col-12 MtClass ">
        <h3 class="mt-1 text-center"><strong>{{ __('ui.homeCategorieH3')}}</strong></h3>
      {{--   <p class="text-center">{{ __('ui.homeDescCategorie')}}</p> --}}
        <div class="container text-center my-3">
        </div>
      </div>
    </div>

     
  
  <div class="container-fluid bgWhite">
    <div class="container">
      <div class="carousel-container_c">
        <div class="carousel-inner_c">
          <div class="track">
            @foreach ($categories as $category)
            <div class="card-container">
              <div class="card_custom">
                <div class="img "><div  class="bgCategory col-6 col-md-2 px-3 m-3 category_box  d-flex flex-column align-items-center justify-content-center text-center shadow">
                  <a class="nav-link  " href="{{route('category_show', compact('category'))}}">
                    <h5 >{{$category->name}}</h5>
                  </a>
                  
                  
                </div>
              </div>
              
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="nav">
        <button class="prev">
          <i class="material-icons">
            keyboard_arrow_left
          </i>
        </button>
        <button class="next">
          <i class="material-icons">
            keyboard_arrow_right
          </i>
        </button>
      </div>
    </div>
    
  </div>
</div>


  </div>


 


           {{-- Comunicazione Home --}}

           <div class="container-fluid text-center">
            <div class="container p-2">
              <div class="row pb-5">
                <h2 class="mb-5">{{__('ui.homePlus')}}</h2>
                <div class="col-md-6">
                  <div class="h-100 p-5 text-white navColor rounded-3 cardComunic">
                    <h2 class="mb-3"><strong>{{__('ui.homePagaH2')}}</strong></h2>
                    <h6 class="text-center my-3">{{__('ui.homePagaDesc')}}</h6>
                    <h6 class="text-center my-3">{{__('ui.homePagaDesc2')}}</h6>
                    <h6 class="text-center my-3">{{__('ui.homePagaDesc3')}}</h6>
                    {{-- <button class="btn btn-outline-light" type="button">Example button</button> --}}
                  </div>
                </div>
                <div class="col-md-6 mt-5 mt-md-0">
                  <div class="h-100 p-5 bg-light border rounded-3 cardComunic">
                    <h2 class="mb-3"><strong>{{__('ui.homeVendiH2')}}</strong></h2>
                    <h6 class="text-center my-3">{{__('ui.homeVendiDesc')}}</h6>
                    <h6 class="text-center my-3">{{__('ui.homeVendiDesc2')}}</h6>
                    <h6 class="text-center my-3">{{__('ui.homeVendiDesc3')}}</h6>
                    {{-- <button class="btn btn-outline-secondary" type="button">Example button</button> --}}
                  </div>
                </div>
              </div>
            </div>
           </div>
          
            
      
      
     
 
    
    {{--comunicazione annunci  --}}
    <div class="container-fluid bgWhite pt-5">
      <div class="row">
        <div class="col-12">
          <div class="container pb-5">
            <div class="row">
              <div class="col-12">
                <h3 class="mt-1 text-center"><strong>{{__('ui.homeUltimiAnnunciH3')}}</strong></h3>
               {{--  <p class="text-center">{{__('ui.homeUltimiAnnunciDesc')}}</p> --}}
                <div class="container text-center my-3">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    {{-- ultimi 4 annunci --}}
    
    <div class="container-fluid bgWhite pb-5">
      <div class="row">
        <div class="col-12">
          <div class="container">
            <div class="row">
              
              @foreach ($announcements as $announcement)
              <div class="col-md-6 ">
                <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative ">
                  <div class="col-auto d-none d-lg-block  ">
                    @if($announcement->images->first())
                    <img class="image-fluid" width="200px" src="{{$announcement->images->first()->getUrl(200,250)}}" alt="">
                    
                    @else
                    
                    <img height="250px" width="200px" src="\img\searchPH.jpeg" alt="" >
                    
                    @endif 
                  </div>
                  
                  <div class="col p-4 d-flex flex-column position-static">
                    <strong class="d-inline-block mb-2 catColor">{{$announcement->category->name}}</strong>
                    <h5 class="mb-0"><strong>{{substr($announcement->title, 0, 25 )}}</strong></h5>
                    <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} <span><strong> -</strong> {{$announcement->price}} €</span>
                    </div>
                    <p class="card-text mb-auto">{{substr($announcement->body, 0, 100 )}}... </p>
                    <a class="mt-3" href="{{route('detailAnnouncement', compact('announcement'))}}" class="stretched-link"><button class="btn btnOrange">{{__('ui.btnScopri')}}</button></a>
                  </div>

                </div>
              </div>
              @endforeach
 
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="container-fluid navColor p-5">
      <div class="container p-3">
        <div class="row">
          <div class="col-12 col-md-6 offset-md-3 text-center">
            <form method="POST" action="{{route('newsletterSubmit')}}">
              @csrf
              <div class="my-3">  
                <h2 class="text_white ">{{__('ui.HomeH2NewsL')}}</h2>
                  <label for="exampleInputText1"><h6 class="text_white  p-2">{{__('ui.HomeNewsLdesc')}}</h6> </label>

                  <input type="email" name="email" placeholder="Indirizzo email" class="form-control">
              </div>

              <button type="submit" class="btn btn_custom_out">{{__('ui.btnHomeSubmit')}}</button>
          </form>

          </div>
        </div>
      </div>

    </div>
    
    
    




    
    
    
    
    {{-- <script src="https://kit.fontawesome.com/b22f715c56.js" crossorigin="anonymous"></script> --}}
    
    
    <script>
      const prev = document.querySelector('.prev');
      const next = document.querySelector('.next');
      
      const track = document.querySelector('.track');
      
      let carouselWidth = document.querySelector('.carousel-container_c').offsetWidth;
      
      window.addEventListener('resize', () => {
        carouselWidth = document.querySelector('.carousel-container_c').offsetWidth;
      })
      
      let index = 0;
      
      next.addEventListener('click', () => {
        index++;
        prev.classList.add('show');
        track.style.transform = `translateX(-${index * carouselWidth}px)`;
        
        if (track.offsetWidth - (index * carouselWidth) < carouselWidth) {
          next.classList.add('hide');
        }
      })
      
      prev.addEventListener('click', () => {
        index--;
        next.classList.remove('hide');
        if (index === 0) {
          prev.classList.remove('show');
        }
        track.style.transform = `translateX(-${index * carouselWidth}px)`;
      })
    </script>
    
    
  </x-layout>