<x-layout>

    <div class="container  pt-2 MtClass">
        
      <div class="row">
          <div class="col-12 mb-5">
              <h3 class="mt-1 text-center"><strong>{{ __('ui.MyAnn')}}</strong></h3>
          </div>
      </div>
      
  </div>
  
       {{-- box i miei annunci--}}
       <div class="container MbClass wide-50">
        <div class="row">
           
            @if ($announcements->first())
            <div class="col-12 col-md-6 offset-md-3 text-center my-3">              
                <h6 class="ms-3">{{count($announcements)}} risultati trovati</h6>
            </div>

            @else 
                <h3 class="ms-3 text-center">Nessun annuncio trovato</h3>      
            @endif

          @foreach ($announcements as $announcement)
              
            <div class="col-12 ">
                <div class="card bgWhite mb-3 ps-4 p-3 shadow " >
                    <div>
                        <div class="row">
                            <div class=" col-12 col-md-8 pb-0 mb-0 d-flex align-items-center">
                                
                                <div>
                                    <strong class="d-inline-block tBlue">{{$announcement->category->name}}</strong>
                                    <h5 class="pb-0 mb-0">{{substr($announcement->title, 0, 20 )}} </h5>
                                    <p class="p-0 my-1">{{$announcement->created_at->format('d/m/Y')}} </p>
                                </div>

                            </div>
    
                            <div class="col-12 col-md-4 d-flex justify-content-center justify-content-md-end p-3">

                              <form method="POST" action="{{route('delete', compact('announcement'))}}">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btnDel m-1">Elimina</button>
                            </form>
 
                              <a class="m-0 p-0" href="{{route('edit', compact('announcement'))}}"><button class="btn btnMod m-1">Modifica</button></a>

                              

                               <a class="m-0 p-0" href="{{route('detailAnnouncement', compact('announcement'))}}"> <button class="btn btnVedi m-1">Dettagli</button> </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
  
  
  
  </x-layout>