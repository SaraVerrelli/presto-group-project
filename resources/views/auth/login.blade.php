<!doctype html>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- csrf laravel --}}
    <meta name="csrf-token" content="{{csrf_token()}}">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- Google fonts --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Urbanist:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    
    <link rel="shortcut icon" href="/faviconSito.png" type="image/x-icon"/>

  





    
    
    
    {{-- Creo un segnaposto per ospitare il title della pagina restituita dalla vista, utilizzo un operatore ternario per gestire l'eventuale assenza del title --}}
    <title>{{$title ?? ''}}</title>



    
</head>

<body>
    
    <x-navbar/> {{-- Importo la navbar dal file navbar.blade --}}
    
    <div class="container" id="container">
    
      <div class="form-container sign-up-container">
          <form  method="POST" class="form_lr" action="{{route('register')}}">
              @csrf
              <h1>Crea il tuo account</h1>
              <span class="mb-3">Inizia subito a fare affari!</span>
             
              <div class="w-75">
              <input type="name" class="loginLabel" name="name" placeholder="Nome utente"/> 
              <input type="email" class="loginLabel"  name="email" placeholder="E-mail"/>
              <input type="password" class="loginLabel" name="password" placeholder="Password" />
              <input type="password" class="loginLabel"  name="password_confirmation" placeholder="Conferma password" />
              </div>

              <button type="submit" class="button_lr mt-3">Registrati</button>
          </form>
      </div>


      <div class="form-container sign-in-container">
          <form method="POST" class="form_lr" action="{{route('login')}}">
              @csrf
              <h1>Accedi</h1>
        
              <span class="mb-3">Inserisci i tuoi dati.</span>
              <div class="w-75">
                <input type="email" class="loginLabel" name="email" placeholder="E-mail" />
                <input type="password" class="loginLabel" name="password" placeholder="Password" />
              </div>
              {{-- <a href="#">Password dimenticata?</a> --}}
              <button type="submit" class="button_lr mt-3">Accedi</button>
          </form>
      </div>


      <div class="overlay-container">

          <div class="overlay">
            
              <div class="overlay-panel overlay-left">
                  <h1>Login</h1>
                  <p><b>Per rimanere in contatto con noi, effettua il login con le tue informazioni personali</b></p>
                  <button class="ghost button_lr" id="signIn">Sign In</button>
              </div>

              <div class="overlay-panel overlay-right">
                  <h1>Non hai ancora un account?</h1>
                  <p><b>Inserisci i tuoi dati ed entra subito in Presto.it</b></p>
                  <button class="ghost button_lr" id="signUp">Registrati</button>
              </div>
          </div>
      </div>
      </div>


{{-- <!-- partial -->
<script  src="./script.js"></script> --}}

<script>

const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
container.classList.remove("right-panel-active");
});
</script>
    
    



 
   
    
    {{-- script bootstrap --}}
    <script src="{{asset('js/app.js')}}"></script>

    {{-- jquery --}}
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
    
</body>
</html>



{{-- <x-layout>
  <div class="container-fluid bgWhite p-5">
  <div class="container ps-md-0 bgWhite MtClass">
    <div class="row g-0">
      <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image">

        <img src="https://picsum.photos/800/1000" alt="" class="img-fluid">

      </div>
      <div class="col-md-8 col-lg-6 my-2">
        <div class="login d-flex align-items-center pt-5">
          <div class="container mt-5">
            <div class="row mt-5">
              <div class="col-md-9 col-lg-8 mx-auto">
                <h2 class="login-heading mb-4">{{__('ui.bentornato')}}</h2>
  
                <!-- Sign In Form -->
                <form method="POST" action="{{route('login')}}">
                  @csrf
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label form-txt">{{__('ui.insertEmail')}}</label>
                    <input type="email" class="form-control" name="email">                     
                  </div>
                      
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label form-txt">{{__('ui.insertPass')}}</label>
                    <input type="password" class="form-control" name ="password">                     
                  </div> 
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif                   
                  <button type="submit" class="btn btn_custom">{{__('ui.accedi')}}</button>
                </form>
                <p class="text-center form-txt my-5">{{__('ui.notRegister')}}<a href="{{route('register')}}">{{__('ui.cliccaqui')}}</a> </p>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



 
</x-layout> --}}