<x-layout>
  <div class="container-fluid bgWhite p-5">
<div class="container ps-md-0 bgWhite MtClass">
    <div class="row g-0">
      <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image">

        <img src="https://picsum.photos/800/1000" alt="" class="img-fluid">

      </div>
      <div class="col-md-8 col-lg-6 my-2">
        <div class="login d-flex align-items-center pt-5">
          <div class="container mt-5">
            <div class="row mt-5">
              <div class="col-md-9 col-lg-8 mx-auto">
                <h2 class="login-heading mb-4">{{__('ui.welcome')}}</h2>
  
                <!-- Sign In Form -->
                <form method="POST" action="{{route('register')}}">
                  @csrf
                  
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label form-txt">{{__('ui.insertName')}}</label>
                    <input type="name" class="form-control " name="name">                     
                  </div>
                  
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label form-txt">{{__('ui.insertEmail')}} </label>
                    <input type="email" class="form-control " name="email">                     
                  </div>
                  
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label form-txt">{{__('ui.insertPass')}}</label>
                    <input type="password" class="form-control " name ="password">                     
                  </div>
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label form-txt">{{__('ui.confPass')}}</label>
                    <input type="password" class="form-control " name ="password_confirmation">                       
                  </div>
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  @endif 
                  <button type="submit" class="btn btn_custom">{{__('ui.register')}}</button>
                </form>
                <p class="text-center form-txt my-5">{{__('ui.registrato')}}<a href="{{route('login')}}">Login.</a> </p>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  




  </div>
  
 
</x-layout>



