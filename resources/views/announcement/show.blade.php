<x-layout>
  
  
  <div class="container MtClass wide-50">
    
    <div class="row my-5">
      <div class="col-12 col-md-4 mt-5">
 
        @if($announcement->images->first())
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            @foreach ($announcement->images as $image)
              @if ($image == $announcement->images->first())
              <div class="carousel-item active">
                <img class="image-fluid" width="420px" src="{{$image->getUrl(400,300)}}" alt="">
              </div>
              @else
              <div class="carousel-item">
                <img class="image-fluid" width="420px" src="{{$image->getUrl(400,300)}}" alt="">
              </div>                
              @endif
            @endforeach
          </div>

          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>

        </div>
          
          
          @else
          
          <img height="300px" width="400px" src="\img\searchPH.jpeg" alt="" >
          
          @endif 
          
        </div>
        
        <div class="col-12 col-md-5 mt-5 px-5">
          <strong class="d-inline-block mb-2 text-primary">{{$announcement->category->name}}</strong>
          <h1 class="mb-0"><strong>{{$announcement->title}}</strong></h1>
          <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} </div>
          
          <h3>{{$announcement->price}} €</h3>
          
          <p class="card-text mt-4">{{$announcement->body}}</p>
          
          
          
          
          
          
        </div>
        
        <div class="col-12 col-md-3 mt-5 p-3 ">
          <div class="card_venditore">
            <h5>{{__('ui.infovenditore')}}</h5>
            <h6 class="mt-4">{{$announcement->user->name}}</h6>
            
            <p>{{$announcement->user->email}}</p>
            <p> <em>{{__('ui.venditoredal')}} {{$announcement->user->created_at->format('d/m/Y')}}</em></p>
            
            {{--  <a class="mt-3" href="#" class="stretched-link"><button class="btn btn-primary">Contatta</button></a> --}}
             <a class="mt-3" href="{{route('messageCreate', $announcement->user_id)}}" class="stretched-link"><button class="btn btn-primary">Contatta</button></a>
            
          </div>
          
        </div>
        
        
      </div>
    </div>

    
  {{--   <div class="container-fluid ">
      <div class="container p-2">
        <div class="row p-5">
          <div class="col-md-2">
            <i class="fas fa-shield-alt iconePlus"></i>
          </div>
          <div class="col-md-4">
            <h4 class="mb-3">Paga in tutta sicurezza</h4>
          </div>
          <div class="col-md-2">
            <i class="fas fa-people-carry iconePlus"></i>
          </div>
          <div class="col-md-4">
            <h4 class="mb-3">Vendi gratuitamente</h4>
          </div>
        </div>
      </div>
     </div>
 --}}


    
    <div class="container-fluid  bg-white p-5">
      <div class="row">
        <div class="col-12">
          <div class="container">
         
           @if (count($announcement->category->announcements) >=2)

              <div class="row justify-content-around">
              <h2 class="my-3">Ultimi annuncio per: {{$announcement->category->name}}</h2>
              

              @foreach ($announcements as $announcement_i)
              
              @if ($announcement_i->id == $announcement->id)
              
              @else
              <div class="col-12 col-md-3 mt-3 ">
                <div class="row g-0 border rounded overflow-hidden  mb-4 shadow-sm  position-relative minHeight">
                  <div class="col-auto d-none d-lg-block  ">
                    @if($announcement_i->images->first())
                    <img class="image-fluid" width="300px" src="{{$announcement_i->images->first()->getUrl(300,300)}}" alt="">
                    
                    @else
                    
                    <img height="300px" src="\img\searchPH.jpeg" alt="" width="300px">
                    
                    @endif
                  </div>
                  
                  <div class="col p-4 d-flex flex-column position-static">
                    <strong class="d-inline-block mb-2 text-primary">{{$announcement_i->category->name}}</strong>
                    <h5 class="mb-0"><strong>{{$announcement_i->title}}</strong></h5>
                    <div class="mb-1 text-muted">{{$announcement_i->created_at->format('d/m/Y')}} <span><strong> -</strong> {{$announcement_i->price}} €</span>
                    </div>
                    
                    <a class="mt-3" href="{{route('detailAnnouncement', compact('announcement'))}}" class="stretched-link"><button class="btn btn-primary">{{__('ui.btnScopri')}}</button></a>
                  </div>
                  
                </div>
              </div>
              
              @endif
              @endforeach
              
            </div>


            @endif

            
          </div>
        </div>
      </div>
      
    </div>
    
    
    
    
    
    
  </x-layout>