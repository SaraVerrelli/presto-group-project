<x-layout>
  
  {{-- Risultsti di ricerca --}}
  <div class="container MtClass MbClass">
    <div class="row  g-4">
      
      <div class="col-12 col-md-3 stickyCat">
        <div class="col-12  bgWhite shadow Bradius p-4">
          
              
              <form class="" action="{{route('search')}}" method="GET" >
                <input name="q" type="text" placeholder="{{ __('ui.homeBtnCerca')}}" class="form-control me-2">
                <button class="btn btnVedi mt-1" type="submit">{{ __('ui.homeBtnCerca')}}</button>
              </form>
              <h6 class="mt-4"><strong>Ricerca per categoria</strong></h6>
              <ul class="categoryItem">

                @foreach ($categories as $category)
                <li class="categoryItem" >
                  <a class="link-cat" href="{{route('category_show', compact('category'))}}">{{$category->name}}</a>
                  
                </li>
                
                @endforeach
  
              </ul>
   
            </div>
          </div>
          
          
          
          <div class="col-12 col-md-9 offset-md-3 MbClass_cust">
            @if(!$announcements->first())
            
              <h3 class="text-color text-center m-5">Non ci sono annunci per questa ricerca</h6>

                @else

                <h6>{{count($announcements)}} risultati trovati</h6>
            
              @endif
           
            @foreach ($announcements as $announcement)
            
            
            {{-- card risultati --}}
           
            
            <div class="col-12">
              <div class="row g-0 border Bradius mb-4 shadow bgWhite">
                
                
                <div class="p-3 pb-0 d-md-flex  ">
                  
                  <div class="m-2 mb-0">
                    @if($announcement->images->first())
                      <img class="image-fluid" width="300px" src="{{$announcement->images->first()->getUrl(300,150)}}" alt="">
                    @else
                      <img height="150px" src="\img\searchPH.jpeg" alt="" width="300px">
                    @endif   
                  </div>
                       
                  <div class="ps-3">
                    {{-- nome annuncio --}}
                    <h5 class="pt-2">
                      
                      <strong> {{substr($announcement->title, 0, 50 )}} </strong>
                      
                    </h5>
                    <div class="">
                      <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} <span><strong class=" mb-2 text-primary">{{$announcement->category->name}} </strong></span></div>
                    </div>
                    
                    
                    <p class="card-text mb-4">{{substr($announcement->body, 0, 100 )}}... </p>
                  </div>
                  
                </div>
                
                <div class="d-flex justify-content-between align-items-center ">
                  
                  <div class="ps-4">
                    <p class="pt-2 pe-4"><strong>Inserzionista: </strong>{{$announcement->user->name}}</p>
                    
                  </div>
                  
                  <div class="d-flex ps-4 pb-3 pb-0 align-items-center d-flex justify-content-end">
                    
                    <div>
                      <h4 class="pt-2 pe-4"><strong>€ {{$announcement->price}}</strong></h4>
                    </div>
                    
                    <div class="pe-4">
                      
                      {{--    <button class="btn btn-primary  ">Vedi Annuncio</button> --}}
                      
                      <a href="{{route('detailAnnouncement', compact('announcement'))}}" class=""><button class="btn btnVedi">Scopri di Più</button></a>
                      
                    </div> 
                  </div>
                  
                  
                  
                  
                  
                </div>
                
              </div> 
              
              
            </div>
            
            @endforeach
            
        
            
            
          </div>
          
        </div>
        
        
        
      </div> 
      
      
      
    </x-layout>