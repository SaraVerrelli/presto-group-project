<x-layout>
    
    
    
    <div class="container"> 
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                @if (session('message')) 
                <div class="alert alert-success"> 
                    {{ session('message') }}
                </div>       
                @endif
            </div>
        </div>
    </div>
      
    <div class="container  pt-2 MtClass ">      
        <div class="row">
            <div class="col-12 mb-5">
                <h3 class="mt-1 text-center"><strong>{{ __('ui.RevHh3')}}</strong></h3>
            </div>
        </div>     
    </div>
    
   
    @if ($announcement == null)
    <div class="container p-5 mt-2">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center my-3">
                <h3 >{{ __('ui.RevHnoAds')}}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center my-3">
                <p>{{ __('ui.RevHp')}}</p>
                <a href="{{route('home')}}">
                    <button class="btn btn_custom">{{__('ui.RevHBtn')}}
                    </button>
                </a>
            </div>
        </div>
    </div>
    @else
    
    {{-- Annunci da revisionare --}}
    <div class="container ">
        <div class="row">
            <div class="col-12 ">
                <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative bgWhite">                  
                    <div class="col p-4 d-flex flex-column position-static  wide-50">                       
                        
                        {{-- nome annuncio --}}
                        <h5 class="mb-0v lh-1"><span><strong class="d-inline-block mb-2 text-primary">{{$announcement->id}} - </strong></span><strong> {{$announcement->title}}</strong></h5>
                            <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} - <span><strong class="d-inline-block mb-2 text-primary">{{$announcement->category->name}}</strong></span></div>
                                <h6>{{__('ui.Price')}}{{$announcement->price}} €</h6>
                                <p class="card-text mb-4">{{$announcement->body}}</p>                        
                                <div class="col-12">
                                    @if($announcement->images->first())
                            
                                    @foreach ($announcement->images as $image)
                                    <div class="col-12 card p-3 mb-3">
                            
                                    <div class="d-sm-flex">
                                            <div class=" ">
                                                <img class="image-fluid" width="300px" src="{{$image->getUrl(300,150)}}" alt="">
                                            </div>
                                    
                                    
                                        <div  class="d-md-flex">
                                            <div class="mt-2 me-2 pt-0 ps-3 p-2 border-md-bottom border-end d-flex align-items-center">
                           
                                            <div class="me-4">
                                                @if ($image->adult == "VERY_UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Adult</strong></h6>
                                                </div>
                                                @elseif ($image->adult == "UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Adult</strong></h6>
                                                </div>
                                                @elseif ($image->adult == "POSSIBLE")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW "></div>
                                                    <h6 class="ps-2"><strong>Adult</strong></h6>
                                                </div>
                                                @else
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-danger"></div>
                                                    <h6 class="ps-2"><strong>Adult</strong></h6>
                                                </div>
                                                @endif

                                                @if ($image->spoof == "VERY_UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Spoof</strong></h6>
                                                </div>
                                                @elseif ($image->spoof == "UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Spoof</strong></h6>
                                                </div>
                                                @elseif ($image->spoof == "POSSIBLE")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW"></div>
                                                    <h6 class="ps-2"><strong>Spoof</strong></h6>
                                                </div>
                                                @else
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-danger"></div>
                                                    <h6 class="ps-2"><strong>Spoof</strong></h6>
                                                </div>
                                                @endif

                                                @if ($image->medical == "VERY_UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Medical</strong></h6>
                                                </div>
                                                @elseif ($image->medical == "UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Medical</strong></h6>
                                                </div>
                                                @elseif ($image->medical == "POSSIBLE")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW"></div>
                                                    <h6 class="ps-2"><strong>Medical</strong></h6>
                                                </div>
                                                @else
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-danger"></div>
                                                    <h6 class="ps-2"><strong>Medical</strong></h6>
                                                </div>
                                                @endif

                                                @if ($image->violence == "VERY_UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Violence</strong></h6>
                                                </div>
                                                @elseif ($image->violence == "UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Violence</strong></h6>
                                                </div>
                                                @elseif ($image->violence == "POSSIBLE")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW"></div>
                                                    <h6 class="ps-2"><strong>Violence</strong></h6>
                                                </div>
                                                @else
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-danger"></div>
                                                    <h6 class="ps-2"><strong>Violence</strong></h6>
                                                </div>
                                                @endif

                                                @if ($image->racy == "VERY_UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Racy</strong></h6>
                                                </div>
                                                @elseif ($image->racy == "UNLIKELY")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-success"></div>
                                                    <h6 class="ps-2"><strong>Racy</strong></h6>
                                                </div>
                                                @elseif ($image->racy == "POSSIBLE")
                                                <div class="d-flex">
                                                    <div class="CircleStatusW"></div>
                                                    <h6 class="ps-2"><strong>Racy</strong></h6>
                                                </div>
                                                @else
                                                <div class="d-flex">
                                                    <div class="CircleStatusW bg-danger"></div>
                                                    <h6 class="ps-2"><strong>Racy</strong></h6>
                                                </div>
                                                @endif


                                            </div> 
                                          
                                            
                                        
                                        
                                        </div>                                                                             
                                        
                                            <div class="ps-3 p-2 ">
                                                <h6><strong>Keywords Individuate</strong></h6>
                                                @if($image->labels)
                                                    @foreach ($image->labels as $label)
                                                    <span>{{$label}} <strong>-</strong></span>
                                                    @endforeach
                                                @endif                                                                               
                                            </div>
                                        </div>
                                    </div>
                        
                            </div>
                            @endforeach 
                        
                 
                   
                         @else
                            <p>nessuna immagine presente</p>
                         @endif
                        </div>
                        
                    </div>
                    
                    
                    
                    <div class="col-12 d-flex justify-content-end p-3">
                        <form method="POST" action="{{ route('revisorReject', $announcement->id) }}">
                            @csrf
                            <button class="btn btnDel m-2">{{__('ui.Rifiuta')}}</button>
                        </form>
                        
                        <form method="POST" action="{{ route('revisorAccept', $announcement->id) }}">
                            @csrf
                            <button class="btn btnVedi m-2">{{__('ui.Accetta')}}</button>
                        </form>
                        
                    </div> 
                    
                    
                </div>
            </div>
            
        </div>
    </div>

        
    @endif
        
        
    {{-- Box Recupero Annunci--}}
        <div class="container">
            <div class="row">
                
                {{-- Recupero Approvati --}}
                <div class="col-12 col-md-6 ">
                    
                    <h5 class="mt-1 mb-4 text-center"><strong>{{__('ui.RecApp')}}</strong></h5>
                    
                    <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative bgWhite">
                        
                        
                        <div class="col p-4 d-flex flex-column position-static">
                            @foreach ($announcementRev as $announcement)
                            
                            <div class="row border-bottom my-3">
                                <div class="col-12 col-md-8">
                                    <h6 class="mb-0v lh-1">
                                        <span><strong class="d-inline-block mb-2 text-primary">{{ $announcement->id }} - </strong></span>
                                        <strong>{{$announcement->title}}</strong>
                                    </h6>
                                    <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} 
                                        <span><strong class="d-inline-block mb-2 text-primary">{{$announcement->category->name}} </strong> - {{$announcement->price}}€</span>
                                    </div>
                                </div> 
                                <div class="col-12 col-md-4">
                                    <span class="ms-auto inline-block">
                                        <form method="POST" action="{{ route('revisorUndo', $announcement->id) }}">
                                            @csrf                                       
                                            <button class="btn btnMod m-2" type="submit">{{__('ui.Recupera')}}</button>
                                        </form>
                                    </span>
                                </div>
                                
                            </div>
                            
                            
                            
                            @endforeach
                        </div>
                        
                    </div>
                    
                    
                </div>
                
                
                
                
                
                
                
                
                
                
                
                
                {{-- Recupero Rifiutati --}}
                <div class="col-12 col-md-6 ">
                    
                    <h5 class="mt-1 mb-4 text-center"><strong>{{__('ui.RecRif')}}</strong></h5>
                    
                    <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative bgWhite">
                        
                        
                        <div class="col p-4 d-flex flex-column position-static">
                            @foreach ($announcementRevRej as $announcement)
                            <div class="row border-bottom my-3">
                                <div class="col-12 col-md-8">
                                    <h6 class="mb-0v lh-1">
                                        <span><strong class="d-inline-block mb-2 text-primary">{{ $announcement->id }} - </strong></span>
                                        <strong>{{$announcement->title}}</strong>
                                    </h6>
                                    <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} 
                                        <span><strong class="d-inline-block mb-2 text-primary">{{$announcement->category->name}} </strong> - {{$announcement->price}}€</span>
                                    </div>
                                </div> 
                                <div class="col-12 col-md-4">
                                    <span class="ms-auto inline-block">
                                        <form method="POST" action="{{ route('revisorUndo', $announcement->id) }}">
                                            @csrf                                       
                                            <button class="btn btnMod  m-2" type="submit">{{__('ui.Recupera')}}</button>
                                        </form>
                                    </span>
                                </div>
                                
                            </div>
                            
                            @endforeach
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>
        </div>
        
        
</x-layout>
    