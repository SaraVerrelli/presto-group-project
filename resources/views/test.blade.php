<!doctype html>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- csrf laravel --}}
    <meta name="csrf-token" content="{{csrf_token()}}">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- Google fonts --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Urbanist:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    
    <link rel="shortcut icon" href="/faviconSito.png" type="image/x-icon"/>

  





    
    
    
    {{-- Creo un segnaposto per ospitare il title della pagina restituita dalla vista, utilizzo un operatore ternario per gestire l'eventuale assenza del title --}}
    <title>{{$title ?? ''}}</title>



    
</head>

<body>
    
    <x-navbar/> {{-- Importo la navbar dal file navbar.blade --}}
    
    <div class="container" id="container">
    
      <div class="form-container sign-up-container">
          <form  method="POST" class="form_lr" action="{{route('register')}}">
              @csrf
              <h1>Crea il tuo account</h1>
              <span class="mb-3">Inizia subito a fare affari!</span>
             
              <div class="w-75">
              <input type="name" class="loginLabel" name="name" placeholder="Nome utente"/> 
              <input type="email" class="loginLabel"  name="email" placeholder="E-mail"/>
              <input type="password" class="loginLabel" name="password" placeholder="Password" />
              <input type="password" class="loginLabel"  name="password_confirmation" placeholder="Conferma password" />
              </div>

              <button type="submit" class="button_lr mt-3">Registrati</button>
          </form>
      </div>


      <div class="form-container sign-in-container">
          <form method="POST" class="form_lr" action="{{route('login')}}">
              @csrf
              <h1>Accedi</h1>
        
              <span class="mb-3">Inserisci i tuoi dati.</span>
              <div class="w-75">
                <input type="email" class="loginLabel" name="email" placeholder="E-mail" />
                <input type="password" class="loginLabel" name="password" placeholder="Password" />
              </div>
            
              <button type="submit" class="button_lr mt-3">Accedi</button>
          </form>
      </div>


      <div class="overlay-container">
          <div class="overlay">
              <div class="overlay-panel overlay-left">
                  <h1>Login</h1>
                  <p><b>Per rimanere in contatto con noi, <br> effettua il login con le tue informazioni personali</b></p>
                  <button class="ghost button_lr" id="signIn">Sign In</button>
              </div>

              <div class="overlay-panel overlay-right">
                  <h1>Non hai ancora un account?</h1>
                  <p><b>Inserisci i tuoi dati ed entra subito in Presto.it</b></p>
                  <button class="ghost button_lr" id="signUp">Registrati</button>
              </div>
          </div>
      </div>
      </div>


{{-- <!-- partial -->
<script  src="./script.js"></script> --}}

<script>

const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
container.classList.remove("right-panel-active");
});
</script>
    
    



 
   
    
    {{-- script bootstrap --}}
    <script src="{{asset('js/app.js')}}"></script>

    {{-- jquery --}}
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
    
</body>
</html>





<script>

const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
container.classList.remove("right-panel-active");
});
</script>




 

