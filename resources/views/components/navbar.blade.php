<nav class="navbar navbar-expand-md navbar-dark navColor fixed-top" aria-label="Fourth navbar example">
  <div class="container">
    
    <a class="navbar-brand" href="{{route('home')}}"><img src="/img/Loghi-Presto/PNG/Logo-Presto-whiteSito.png" alt="" height="20"></a>
    
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarsExample04">
      <ul class="navbar-nav ms-auto mb-2 mb-md-0">
       
        @guest
        <li class="ms-auto"><a class="nav-link" href="{{route('login')}}"><strong>Login</strong></a></li>
        
        @endguest
        
        
        @auth
        
        
        
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-bs-toggle="dropdown" aria-expanded="false">{{__('ui.hello')}} {{Auth::user()->name}} </a>
          <ul class="dropdown-menu" aria-labelledby="dropdown04">
            
            <li><a class="dropdown-item" href="{{route('userAnnouncements')}}">{{__('ui.yourAds')}}</a></li>
            <li><a class="dropdown-item" href="{{route('messageIndex')}}">Messaggi</a></li>
            
            @if (Auth::user()->is_revisor)
            <li class="nav-item">
              <a class="nav-link-color mt-1" href="{{route('revisorHome')}}">{{__('ui.revisorHome')}}<span class="badge">{{App\Models\Announcement::ToBeRevisionedCount()}}</span></a>
              
            </li>
            @else
            <li><a class="dropdown-item" href="{{route('revisorRequest')}}">{{__('ui.becomeRev')}}</a></li>
            @endif
            
            
            <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
              document.getElementById('form-logout').submit();">Logout</a></li>
              <form method="POST" action={{route('logout')}} id="form-logout">
                @csrf
              </form>
              
              
            </ul>
          </li>
          @endauth
          
          
          
        </ul>
        
        <ul class="flagUl d-flex mt-2 p-0">
          
              {{-- <li class="nav-item"><a class="nav-link" href="">
                <span class="flag-icon flag-icon-it">aaa</span></a> 
              </li> --}}
              
              <li class="pt-2"> <form action="{{route('locale', 'en')}}" method="POST">
                @csrf
                <button class="flag" type="submit" class="nav-link">
                  
                  <img width="20px" src="/img/icons/en.svg" alt="">
                  
                </button>
                
              </form>
            </li>
        
            <li class="pt-2">
              <form action="{{route('locale', 'it')}}" method="POST">
                @csrf
                <button class="flag" type="submit" class="nav-link">
                  <img width="20px" src="/img/icons/ita.svg" alt="">
                  
                </button>
              </form>
            </li>
            
            <li class="pt-2">
              <form action="{{route('locale', 'de')}}" method="POST">
                @csrf
                <button class="flag" type="submit" class="nav-link">
                  <img width="20px" src="/img/icons/de.svg" alt="">
                  
                </button>
              </form>
            </li>
            
            
              <li class="nav-item ps-3">
                <a href="{{route('create')}}"><button class="btn  btnAddAnn shadow"><strong>{{__('ui.btnInsAnn')}}</strong></button></a>
              
                
              </li>
        </ul>
        
      </div>
    </div>
  </nav>