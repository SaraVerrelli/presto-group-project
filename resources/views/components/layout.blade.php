<!doctype html>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- csrf laravel --}}
    <meta name="csrf-token" content="{{csrf_token()}}">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- Google fonts --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Urbanist:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    
    <link rel="shortcut icon" href="/faviconSito.png" type="image/x-icon"/>

  





    
    
    
    {{-- Creo un segnaposto per ospitare il title della pagina restituita dalla vista, utilizzo un operatore ternario per gestire l'eventuale assenza del title --}}
    <title>{{$title ?? ''}}</title>



    
</head>

<body>
    
    <x-navbar/> {{-- Importo la navbar dal file navbar.blade --}}
    
    {{-- creo uno slot per poter ospitare il codice delle viste --}}
    {{$slot}}
    
    
    



 
    <x-footer/>
    
    {{-- script bootstrap --}}
    <script src="{{asset('js/app.js')}}"></script>

    {{-- jquery --}}
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
    
</body>
</html>