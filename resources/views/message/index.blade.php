<x-layout>
 

    <div class="container mt-5 wide">
        <div class="row">
            <div class="col-12 mt-5 mb-5">
   
                   {{-- TEST index --}}
                   <div class="container">
                       <div class="row g-1">
                           
               
                               @foreach ($messages_tot as $messages_t_id)           
                               @foreach ($messages_t_id as $message)
                             
                               @if ($loop->first) 
                 
                                   @if ($thisUser == $message->user)
                                       @break
                                   @else                
                                    <div class="row ">
                                       <div class="card p-4  ">
                                           <div class="">
                                               <h6 class="m-0 pe-2 p-0"><strong>{{$message->user->name}}</strong> ti ha contattato</h6>
                                   @endif
                               @endif
   
                               @if ($loop->last)
                                       <div class="col-5">
                                       <p class="m-2"> {{$message->message}}</p> <p class="ms-2">{{$message->created_at->format('d/m/Y H:i')}}</p>
                       
                                           <div class=" mt-2 col-5">
                                           <a href="{{route('messageDetail', $message->user_id)}}"><button class="btn btn_custom">Leggi</button></a>
                                           </div>
              
                                       </div>
                                   </div>
                                   </div>
                               </div>
                       
                               @endif
                                 
                               @endforeach 
                               @endforeach 
                           
                       </div>
                   </div>
   
                
            </div>
        </div>
    </div>
   </x-layout>
   