<x-layout>

    {{-- {{dd($sender, $addressee)}} --}}
    <div class="container-fluid mt-5 bg-light mb-5 ">
       
        <div class="row mx-auto justify-content-center wide mt-5">
            <div class="col-6">
                <form action="{{route('messageSend', compact('sender', 'addressee'))}}" method="POST" class="mt-5">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Scrivi</label>
                        <input type="text" class="form-control" name="message">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Conferma</button>

                </form>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        
                @if (session('message')) 
                <div class="alert alertCustom Bradius col-8 offset-2 mb-5"> 
                  {{ session('message') }}
                </div>       
                @endif
            </div>
            
            
            
        </div>
    </div>
</x-layout>