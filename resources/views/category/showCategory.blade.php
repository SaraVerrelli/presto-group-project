{{-- <x-layout>


  <div class="container-fluid bgWhite shadow wide my-5">
    <div class="row mt-5">
      <div class="col-12 MtClass">
        <div class="container">
          <div class="row">
            <h1 class="my-4">{{__('ui.searchRes')}}{{$category->name}}</h1>
            @foreach ($announcements as $announcement)
            <div class="col-md-6 ">
              <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative ">
                
                <div class="col-auto d-none d-lg-block  ">
                  <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                  
                </div>
                
                <div class="col p-4 d-flex flex-column position-static">
                  <strong class="d-inline-block mb-2 text-primary">{{$announcement->category->name}}</strong>
                  <h5 class="mb-0"><strong>{{substr($announcement->title, 0, 20 )}}</strong></h5>
                  <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} <span><strong> -</strong> {{$announcement->price}} €</span></div>
                  <p class="card-text mb-auto">{{substr($announcement->body, 0, 100 )}}... </p>
                  <a class="mt-3" href="{{route('detailAnnouncement', compact('announcement'))}}" class="stretched-link"><button class="btn btn-primary">{{__('ui.btnScropri')}}</button></a>
                </div>
                
                
                
                
              </div>
            </div>
            @endforeach
        

          </div>
        </div>
      </div>
    </div>
  </div>









</x-layout> --}}

<x-layout>
  
  {{-- Risultsti di ricerca --}}
  <div class="container MtClass MbClass">
    <div class="row g-4">
      
      <div class="col-12 col-md-3 ">
        <div class="col-12  bgWhite shadow Bradius p-4">
          {{-- <div>  <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-success mt-2 vw-25" type="submit">Search</button></div>
            
            <div class="mt-2 mt-md-4"> --}}
              
              <form class="" action="{{route('search')}}" method="GET">
                <input name="q" type="text" placeholder="{{ __('ui.homeBtnCerca')}}" class="form-control me-2">
                <button class="btn btn_custom my-3" type="submit">{{ __('ui.homeBtnCerca')}}</button>
              </form>
              
              
              <h6 class="mt-4"><strong>Ricerca per categoria</strong></h6>
              <ul class="categoryItem">
                @foreach ($categories as $category)
                <li class="categoryItem" >
                  <a class="link-cat" href="{{route('category_show', compact('category'))}}">{{$category->name}}</a>
                  
                </li>
                
                @endforeach
                
                
              </ul>
              
              {{-- 
              </div> --}}
        </div>
      </div>
          
          
          
          <div class="col-12 col-md-9 ">
            @if(!$announcements->first())
              <h3 class="text-color text-center m-5">Non ci sono annunci per questa ricerca</h6>
            
            @endif
            
            @foreach ($announcements as $announcement)
            
            {{-- ¯card risultati --}}
            
            <div class="col-12 ">
              <div class="row g-0 border Bradius mb-4 shadow   bgWhite">
                
                
                <div class="p-3 pb-0 d-md-flex  ">
                  
                  
                  <div class="m-2 mb-0">
                    
                    
                    @if($announcement->images->first())
                    <img class="image-fluid" width="300px" src="{{$announcement->images->first()->getUrl(300,150)}}" alt="">
                    
                    @else
                    <img height="" src="img\searchPH.jpeg" alt="">
                    
                    @endif
                    
                    
                    
                    
                    
                  </div>
                  
                  
                  <div class="ps-3">
                    {{-- nome annuncio --}}
                    <h5 class="pt-2">
                      
                      <strong> {{substr($announcement->title, 0, 50 )}} </strong>
                      
                    </h5>
                    <div class="">
                      <div class="mb-1 text-muted">{{$announcement->created_at->format('d/m/Y')}} <span><strong class=" mb-2 text-primary">{{$announcement->category->name}} </strong></span></div>
                    </div>
                      
                      
                      <p class="card-text mb-4">{{substr($announcement->body, 0, 100 )}}... </p>
                  </div>
                    
                </div>
                  
                <div class="d-flex justify-content-between align-items-center ">
                    
                    <div class="ps-4">
                      <p class="pt-2 pe-4"><strong>Inserzionista: </strong>{{$announcement->user->name}}</p>
                      
                    </div>
                    
                    <div class="d-flex ps-4 pb-3 pb-0 align-items-center d-flex justify-content-end">
                      
                      <div>
                        <h4 class="pt-2 pe-4"><strong>€ {{$announcement->price}}</strong></h4>
                      </div>
                      
                      <div class="pe-4">
                        
                        {{--    <button class="btn btn-primary  ">Vedi Annuncio</button> --}}
                        
                        <a href="{{route('detailAnnouncement', compact('announcement'))}}" class=""><button class="btn btn-primary">Scopri di Più</button></a>
                        
                      </div> 
                    </div>
                    
                    
                    
                    
                    
                </div>
                  
              </div> 
                
                
            </div>
              
            @endforeach
              
          </div>
            
    </div>
          
          
          
  </div> 
 
  
        
</x-layout>