<x-layout>
    <div class="container mt-5">
        <div class="row">
            <div class="col12 mt-5">
                <h2>Messaggi di Luan</h2>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row g-1">

            <div class="col-12 mb-5">
                <div class="card p-4 ">

                    <div class="card p-4 pb-0 msgArea mb-1">
                        <div class="d-flex justify-content-end">
 
                            <p class="m-0 pe-2 p-0"><strong>Ciao Luan sono interessato all'oggetto in vendita</strong></p>
    
                        </div>
    
                        <div class="d-inline-flex justify-content-end  pb-3 mt-2 ">
     
                            <p class="m-0 pe-2 msgStyleMit">L'oggetto è disponibile</p>
    
                        </div>
    
                        <div class="d-flex justify-content-end">
     
                            <p class="m-0 pe-2 p-0"><strong>Come concordiamo l'acquisto?</strong></p>
    
                        </div>

                        <div class="d-inline-flex justify-content-end  pb-3 mt-2 ">
     
                            <p class="m-0 pe-2 msgStyleMit">Pagamento via Paypall</p>
    
                        </div>
    
                    </div>

                  
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                        <label for="floatingTextarea2">Scrivi Messaggio</label>
                    </div>

                    <div class="d-flex justify-content-end pt-3">
                        <button class="btn btn-primary">Invia</button>
                    </div>


                </div>
    
                </div>
            </div>

           

        </div>
    </div>

</x-layout>