<?php
return [
    /* home page */

    'homeH1' => 'Presto, kauf und verkauf deine gebrauchte Artikeln',
    'homeHeaderH3' => "Worauf wartest du?",
    'homeHeaderh32' => 'Finde dein passendes Angebot und fördert das Recycling!',

    'homeHeaderH6' => "UNTERSTÜTZT DIE UMWELT",
    'homeBtnCerca' => 'Finden',

    'homeCategorieH3' => 'KATEGORIEN DURCHSUCHEN',
    'homeDescCategorie' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using ',

    'homePlus' => 'Das beste Angebot ist immer das sichere',

    'homePagaH2' => 'SICHER BEZAHLEN',
    'homePagaDesc' => 'Verwenden Sie PayPal für Online-Zahlungen',
    'homePagaDesc2' => 'Kredit- oder wiederaufladbare Karten ermöglichen Zahlungen in Echtzeit',
    'homePagaDesc3' => 'Sie können sich auch für eine Banküberweisung entscheiden',

    'homeVendiH2' => 'FREI VERKAUFEN',
    'homeVendiDesc' => "Veröffentlichen Sie Ihre Anzeige kostenlos in über 10 Kategorien",
    'homeVendiDesc2' => "Verkauf ohne zusätzliche Kosten, auch beim Verkauf keine Provision",
    'homeVendiDesc3' => "Millionen interessierter Nutzer",


    'homeUltimiAnnunciH3' => 'LETZTE ANGEBOTE',

    'homeUltimiAnnunciDesc' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using',

    'btnScopri' => 'Mehr erfahren',

    'HomeH2NewsL' => 'Jeden tag einen neuen Deal!',
    'HomeNewsLdesc' => 'Gib dein Mail ein, um stets auf dem Laufenden zu bleiben.',

    'btnHomeSubmit' => 'Anmelden',

    /*revisorHome*/

    'MyAnn' => 'DEINE ANZEIGEN',

    'RevHh3' => 'ANZEIGEN ZU ÜBERPRÜFEN',
    'RevHnoAds' => 'Es gibt keine Anzeigen zu überprüfen',
    'RevHp' => 'Zürück zu Homepage',
    'RevHBtn' => 'Home',
    'Price' => 'Preise ',
    'Rifiuta' => 'Ablehnen',
    'Accetta' => 'Akzeptieren',
    'RecApp' => 'Akzeptierte abrufen',
    'RecRif' => 'Abgehlente abrufen',
    'Recupera' => 'Abrufen',

    //components

    'btnInsAnn' => 'Anzeige +',
    'chiSiamo' => 'Über uns',
    'reviews' => 'Bewertungen',
    'register' => 'Registrieren',
    'hello' => 'Hallo',
    'yourAds' => 'Deine Anzeigen',
    'revisorHome' => 'Home Prüfer',
    'becomeRev' => 'Werde Prüfer',

    //showCategory
    'searchRes' => 'Suchergebnisse für: ',
    //dashboard
    'articoliDi' => 'Anzeigen von: ',
    //login
    'bentornato' => 'Willkommen zurück!',
    'insertEmail' => 'Email eingeben',
    'insertPass' => 'Passwort eingeben',
    'accedi' => 'Einloggen',
    'notRegister' => 'Noch nicht registriert?',
    'cliccaqui' => 'Hier klicken',
    //register
    'insertName' => 'Name eingeben',
    'confPass' => 'Passwort bestätigen',
    'registrato' => 'Schon registriert?',
    //RevReq
    'telluswhy' => ', sag uns, wieso du Prüfer werden möchtest.',
    'insertMex' => 'Nachricht eingeben',
    'submit' => 'Hier',
    //create
    'insertNewAd' => 'Neue Anzeige aufgeben',
    'title' => 'Titel',
    'categ' => 'Kategorie',
    'pickCateg' => 'Wähl diene Kategorie',
    'desc' => 'Gib eine kurze Beschreibung',
    'insertPic' => 'Bilder eingeben',
    'save' => 'Speichern',
    'annulla' => 'Abbrechen',
    'infovenditore' => 'Info Verkaufer',
    'venditoredal' => 'Verkaufer seit: ',
    'welcome' => 'Willkommen auf Presto',


























];
