<?php

return [
    /* home page */
    'welcome' => 'Benvenuto!',
    'homeH1' => 'Presto, compra e vendi il tuo usato.',
    'homeHeaderH3' => "Che stai aspettando?",
    'homeHeaderh32' => 'Trova il tuo affare e promuovi il riciclo!',

    'homeHeaderH6' => "SOSTIENI L'AMBIENTE",
    'homeBtnCerca' => 'Cerca',

    'homeCategorieH3' => 'ESPLORA LE CATEGORIE',
    'homeDescCategorie' => 'E una lunga storia It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using ',

    'homePlus' => 'L\'affare migliore è sempre quello sicuro.',

    'homePagaH2' => 'PAGA IN SICUREZZA',
    'homePagaDesc' => 'Utilizza Pay pal per i pagamenti online',
    'homePagaDesc2' => 'Carte di credito o ricaricabili ti permettono di effettuare pagamenti in tempo reale',
    'homePagaDesc3' => 'Puoi optare anche per un bonifico bancario',

    'homeVendiH2' => 'VENDI GRATIS',

    'homeVendiDesc' => "Pubblica il tuo annuncio gratis in più di 10 categorie",
    'homeVendiDesc2' => "Vendita senza costi aggiuntivi, anche quando vendi nessuna commissione",
    'homeVendiDesc3' => "Milioni di utenti interessati",
    
    'homeUltimiAnnunciH3' => 'ULTIMI ANNUNCI INSERITI',

    'homeUltimiAnnunciDesc' => ' descrizione It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using',

    'btnScopri' => 'Scopri di Più',

    'HomeH2NewsL' => 'Ogni giorno un nuovo affare!',
    'HomeNewsLdesc' => 'Inserisci la tua mail per rimanere sempre aggiornato.',

    'btnHomeSubmit' => 'Iscriviti',


    /*revisorHome*/

    'MyAnn' => 'I TUOI ANNUNCI',
    'RevHh3' => 'ANNUNCI DA REVISIONARE',
    'RevHnoAds' => 'Non hai nessun annuncio da revisionare',
    'RevHp' => 'Torna in home',
    'RevHBtn' => 'Home',
    'Price' => 'Prezzo ',
    'Rifiuta' => 'Rifiuta',
    'Accetta' => 'Accetta',
    'RecApp' => 'Recupera approvati',
    'RecRif' => 'Recupera rifiutati',
    'Recupera' => 'Recupera',

    //components

    'btnInsAnn' => 'Inserisci +',
    'chiSiamo' => 'Chi siamo',
    'reviews' => 'Recensioni',
    'register' => 'Registrati',
    'hello' => 'Ciao',
    'yourAds' => 'I tuoi annunci',
    'revisorHome' => 'Home revisore',
    'becomeRev' => 'Diventa revisore',

    //showCategory
    'searchRes' => 'Risultati per: ',
    //dashboard
    'articoliDi' => 'Articoli di: ',
    //login
    'bentornato' => 'Ben tornato!',
    'insertEmail' => 'Inserisci il tuo indirizzo e-mail',
    'insertPass' => 'Inserisci password',
    'accedi' => 'Accedi',
    'notRegister' => 'Non sei registrato?',
    'cliccaqui' => 'Clicca qui',
    //register
    'insertName' => 'Inserisci il tuo nome',
    'confPass' => 'Conferma la password',
    'registrato' => 'Sei già registrato?',
    //RevReq
    'telluswhy' => ', spiegaci brevemente perchè vuoi essere revisore.',
    'insertMex' => 'Inserisci il tuo messaggio',
    'submit' => 'Vai',
    //create
    'insertNewAd' => 'Inserisci un nuovo annuncio',
    'title' => 'Titolo',
    'categ' => 'Categoria',
    'pickCateg' => 'Scegli la categoria',
    'desc' => 'Fai una breve descrizione',
    'insertPic' => 'Inserisci la foto',
    'save' => 'Salva',
    'annulla' => 'Annulla',
    'infovenditore' => 'Info venditore',
    'venditoredal' => 'Venditore dal: '







































];
