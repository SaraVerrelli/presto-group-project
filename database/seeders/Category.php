<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories =['Libri',
       'Musica, Film e TV',
        'Videogiochi e Console',
        'Elettronica e Informatica',
        'Casa, Giardino, Fai da te e Animali',
        'Alimentari e Cura della casa',
       'Bellezza e Salute',
        'Giochi e Prima infanzia',
        'Vestiti, scarpe, gioielli e accessori',
        'Sport e tempo libero',
        'Auto e Moto',
       'Commercio, Industria e Scienza',
    ];

    foreach($categories as $category){
        DB::table('categories')->insert([
            'name' => $category,
        ]);
    }
    }
}
