<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10|max:150',
            'category' => 'required|not_in:0',
            'price' => 'required',
            'body' => 'required|max:800',



        ];
    }

    public function messages()
    {
        return [
            'category.required' => 'Scegli una categoria',
            'category.not_in' => 'Scegli una categoria',
            'title.required' => 'Il titolo è richiesto',
            'title.min' => 'Il titolo deve essere di almeno 10 caratteri.',
            'price.required' => 'Il prezzo è richiesto',
            'body.required' => 'La descrizione è richiesta',



        ];
    }
}
