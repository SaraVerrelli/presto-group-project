<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SetLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $browserLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        if (in_array($browserLang, ['it', 'en', 'de'])) {
            $locale = session('locale', $browserLang);
            App::setLocale($locale);
        } else {
            $locale = session('locale', 'it');
            App::setlocale($locale);
        }



        return $next($request);
    }
}
