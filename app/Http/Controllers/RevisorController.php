<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\View\ViewServiceProvider;

class RevisorController extends Controller
{
    public function __construct(){

        $this->middleware('auth.revisor');
    }

    public function index(){
 
        /* Effettuo una query al Db per richiedere solo il primo annuncio non approvato */
        $announcement = Announcement::where('is_accepted', null)
        ->orderBy('created_at', 'asc')
        ->first();

        $announcementRev = Announcement::where('is_accepted', !null ) 
        ->orderBy('created_at', 'desc')->take(5)->get();

        $announcementRevRej = Announcement::where('is_accepted', 0 ) 
        ->orderBy('created_at', 'desc')->take(5)->get();

        return view('revisor/revisorHome', compact('announcement', 'announcementRev', 'announcementRevRej'));
    }

    private function setAccepted($announcement_id, $value){
        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted = $value;
        $announcement->save();
        return redirect(route('revisorHome'));
    }

    

    public function accept($announcement_id){
        return $this->setAccepted($announcement_id, true);
    }

    public function reject($announcement_id){
        return $this->setAccepted($announcement_id, false);
    }

    public function undo($announcement_id){
        return $this->setAccepted($announcement_id, null);
    }

   


   
}
