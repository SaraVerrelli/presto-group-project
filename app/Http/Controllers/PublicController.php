<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Mail\ContactMail;
use App\Mail\NewsletterMail;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{


    public function index()
    {
        $announcements = Announcement::where('is_accepted', true)
            ->orderBy('created_at', 'desc')
            ->take(4)
            ->get();

        return view('home', compact('announcements'));
    }

    public function showCategory(Category $category)
    {
        $announcements = $category->announcements()->where('is_accepted', true)->get();

        return view('category/showCategory', compact('category', 'announcements'));
    }

    public function test()
    {
        return view('test');
    }

    public function search(Request $request)
    {


        $q = $request->input('q');

        $announcements = Announcement::search($q)->where('is_accepted', true)->get();
        if ($q == null) {
            $announcements = Announcement::where('is_accepted', true)->orderBy('created_at', 'desc')->get();
        }

        return view('announcement/searchResults', compact('q', 'announcements'));
    }


    public function newsletterSubmit(Request $request)
    {
        $email = $request->input('email');

        $contact = compact('email');


        Mail::to('Amministrazione@presto.it')->send(new NewsletterMail($contact));

        return redirect(route('home'))->with('message', 'Ottimo, ti aggiorneremo su tutti i nuovi affari disponibili!');
    }


    public function locale($locale)
    {


        session()->put('locale', $locale);
        return redirect()->back();
    }
}
