<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function revisorRequest(){

        return view('auth/revisorRequest');
    }

    public function revisorSubmit(Request $request){
        $message= $request->input('message');
        $email= Auth::user()->email;
        
        
        $contact = compact('message', 'email');

         Mail::to('Amministrazione@presto.it')->send(new ContactMail($contact));

        return redirect(route('home'))->with('message', 'Grazie per averci contattato');
    }

    public function index()
    {
        $announcements = Auth::user()->announcements()->get();
        return view('auth/dashboard', compact('announcements'));
    }

    public function msgList(){
        return view('msglist');
    }

    public function msgDetail(){
        return view('msgDetail');
    }
    

}
