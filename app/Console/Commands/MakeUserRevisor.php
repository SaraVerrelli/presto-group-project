<?php
/* Abbiamo istruito un comando Artisan per rendere un utente revisore */
namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    /* NOme Comando */
    protected $signature = 'presto:makeUserRevisor';
   

    /**
     * The console command description.
     *
     * @var string
     */

     /* descrizione del comando */
    protected $description = 'Rendi un utente revisore';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /* Richiedo l'email dell'utente che voglio rendere revisore */
        $email = $this->ask("Inserisci l'email dell'utente che vuoi rendere revisore");

        /* Verifico se l'email inserita è presente nel DB */
        $user = User::where('email', $email)->first();

        if(!$user){
            $this->error('Utente non trovato');
            return;
        }

        /* Assegno al record di riferimento nella colonna is-revisor il valore True e lo salvo nel DB */
        $user->is_revisor = true;
        $user->save();
        $this->info("L'utente {$user->name} è ora un revisore");
    }
}
